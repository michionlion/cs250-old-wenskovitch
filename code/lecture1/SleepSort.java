import java.util.concurrent.CountDownLatch;
import java.util.*;
import java.io.*;

public class SleepSort {

	public static ArrayList<Integer> nums = new ArrayList<Integer>();
	public static int listLength = 11;

	public static void sleepSortAndPrint(ArrayList<Integer> nums) {
		final CountDownLatch doneSignal = new CountDownLatch(nums.size());
		for (final int num : nums) {
			new Thread(new Runnable() {
				public void run() {
					doneSignal.countDown();
					try {
						doneSignal.await();
 
						//using straight milliseconds produces unpredictable
						//results with small numbers
						//using 25 here gives a nifty demonstration
						Thread.sleep(num*25);
						System.out.println(num);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}
	public static void main(String[] args) {
		createList(listLength);
		sleepSortAndPrint(nums);
	}

	public static void createList(int listSize) {

		Random randomGenerator = new Random();
		boolean itsOK = false;
		int newInt = 0;

		for (int i = 0; i < listSize; i++) {
			while (!itsOK) {
				newInt = randomGenerator.nextInt(1000);
				if (!nums.contains(newInt)) {
					itsOK = true;
				} //if
			} //while

			itsOK = false;
			nums.add(newInt);
		} //for

	} //createList
}

