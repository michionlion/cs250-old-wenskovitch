import java.util.*;

class StupidSort {

	public static ArrayList<Integer> numbers = new ArrayList<Integer>();
	public static int listSize = 11;

	public static void main(String args[]) {

		int iteration = 0;

		// Populate the random number list
		createList(numbers);

		// While the list is not sorted, try to sort
		while (!isSorted(numbers)) {
			sort(numbers);

			System.out.print("Iteration " + iteration + ": ");
			print(numbers);
			iteration++;
		} //while

		System.out.print("Sorted list found: ");
		print(numbers);

	} //main

	public static void print(ArrayList<Integer> numbers) {
		for (int i = 0; i < listSize; i++) {
			System.out.print(numbers.get(i) + " ");
		} //for
		System.out.print("\n");
	} //print

	public static boolean isSorted(ArrayList<Integer> numbers) {

		for (int i = 0; i < listSize-1; i++) {
			if (numbers.get(i+1) < numbers.get(i)) {
				return false;
			} //if
		} //for

		return true;

	} //isSorted

	public static void sort(ArrayList<Integer> numbers) {
		Collections.shuffle(numbers);
	} //sort

	public static void createList(ArrayList<Integer> numbers) {

		Random randomGenerator = new Random();

		for (int i = 0; i < listSize; i++) {
			numbers.add(randomGenerator.nextInt(1000));
		} //for

	} //createList


} //StupidSort (class)