import java.io.*;
import java.util.*;

public class QuickSort {

	public static ArrayList<Integer> numbers = new ArrayList<Integer>();
	public static int listSize = 11;
	public static int iteration = 0;

   	public static void main(String args[]) {

		// Populate the random number list
		createList(numbers);

		print(numbers, iteration);

      	Quicksort(numbers, 0, listSize-1);

//		print(numbers, iteration);

      	System.out.print("\n");
   	} //main

	public static void swap (ArrayList<Integer> numbers, int x, int y) {
    	int temp = numbers.get(x);
    	numbers.set(x, numbers.get(y));
    	numbers.set(y, temp);
   	} //swap

   	// Reorganizes the given list so all elements less than the first are
   	// before it and all greater elements are after it.
   	public static int partition(ArrayList<Integer> numbers, int f, int l) {
      	int pivot = numbers.get(f);
      	while (f < l) {
         	while (numbers.get(f) < pivot) {
				f++;
			} //while

         	while (numbers.get(l) > pivot) {
				l--;
			} //while

         	swap(numbers, f, l);
      	} //while

      	return f;
   	} //partition

   	public static void Quicksort(ArrayList<Integer> numbers, int f, int l) {
      	if (f >= l) {
			return;
		} //if

      	int pivot_index = partition(numbers, f, l);
      	Quicksort(numbers, f, pivot_index);
      	print(numbers, iteration++);
      	Quicksort(numbers, pivot_index+1, l);
      	print(numbers, iteration++);
   	} //QuickSort


	public static void print(ArrayList<Integer> numbers, int iteration) {
		System.out.print("Iteration " + iteration + ": ");

		for (int i = 0; i < listSize; i++) {
			System.out.print(numbers.get(i) + " ");
		} //for
		System.out.print("\n");
	} //print

	public static void createList(ArrayList<Integer> numbers) {

		Random randomGenerator = new Random();
		boolean itsOK = false;
		int newInt = 0;

		for (int i = 0; i < listSize; i++) {
			while (!itsOK) {
				newInt = randomGenerator.nextInt(1000);
				if (!numbers.contains(newInt)) {
					itsOK = true;
				} //if
			} //while

			itsOK = false;
			numbers.add(newInt);
		} //for

	} //createList



} //QuickSort (class)

