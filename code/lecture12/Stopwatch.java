import java.io.*;
import java.util.*;

public class Stopwatch {

	private final long start;

	public Stopwatch() {
		start = System.currentTimeMillis();
	} //Stopwatch (constructor)

	public double elapsedTime() {
		long now = System.currentTimeMillis();
		return (now - start) / 1000.0;
	} //elapsedTime

} //Stopwatch (class)